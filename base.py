from bs4 import BeautifulSoup
import requests
import logging
import datetime
import os
import validators
import psycopg2


def create_logger(logname):
    log = logging.getLogger(logname)
    log.setLevel(logging.DEBUG)
    format_str = '%(asctime)s [%(lineno)7d] %(levelname)6s - %(funcName)30s - %(message)s'
    # date_format = '%Y-%m-%d %H:%M:%S,'
    formatter = logging.Formatter(format_str)
    if not os.path.exists("logs/" + logname):
        os.makedirs("logs/" + logname)
    fh = logging.FileHandler(filename="logs/" + logname + "/" + datetime.datetime.now().strftime("%d-%m-%y") +
                                      ".log", encoding='utf8')
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    log.addHandler(fh)
    log.debug("create log with level " + str(logging.getLevelName(log.level)))
    return logging.getLogger(logname)


def init():
    global log
    log = create_logger("log")
    log.info('connect to db')
    global conn
    conn = psycopg2.connect(dbname='dbname', user='postgres', password='root', host='localhost')


def parse_fields(url):
    for i in range(1, 100):
        try:
            mainpage_url = url + '?page=' + str(i)
            log.info("get mainpage = " + mainpage_url)
            mainpage = requests.get(mainpage_url)
            if mainpage.status_code != 200:
                raise Exception('bad code')
            soup = BeautifulSoup(mainpage.text, 'html.parser')
            rec_hrefs = list()
            for href in soup.find_all('a', attrs={"class": "fixed-recipe-card__title-link"}):
                if not validators.url(href.get("href")):
                    log.error("not valid url : " + href.get("href"))
                    continue
                rec_hrefs.append(href.get("href"))
            for recipe_url in rec_hrefs:
                try:
                    cursor = conn.cursor()
                    log.info("get recipe page = " + recipe_url)
                    recipe_page = requests.get(recipe_url)
                    if recipe_page.status_code != 200:
                        raise Exception('bad response: ' + str(recipe_page.status_code))
                    recipe_soup = BeautifulSoup(recipe_page.text, 'html.parser')
                    log.debug('get description')
                    desc = recipe_soup.find('div', 'submitter__description').text
                    desc = desc.strip().replace("'", "''").replace('"', '')
                    ingredients = list()
                    log.debug('get ingredients')
                    for span in recipe_soup.find_all('span', 'recipe-ingred_txt added'):
                        ingredients.append(span.text.replace("'", "''"))
                    log.debug('get prep time')
                    prep_time = recipe_soup.find('time', attrs={'itemprop': 'prepTime'}).find('span', 'prepTime__'
                                                                                                      'item--time').text
                    log.debug('get cooktime')
                    cook_time = recipe_soup.find('time', attrs={'itemprop': 'cookTime'}).find('span', 'prepTime__'
                                                                                                      'item--time').text
                    log.debug('get servings')
                    servings = recipe_soup.find('meta', attrs={'id': 'metaRecipeServings'}).get('content')
                    steps = list()
                    log.debug('get steps')
                    for step in recipe_soup.find_all('li', 'step'):
                        steps.append(step.find('span', 'recipe-directions__list--item').text.strip())
                    log.debug('get img url')
                    img_url = recipe_soup.find('img', 'rec-photo').get('src')
                    log.debug('get title')
                    title = recipe_soup.find('h1', 'recipe-summary__h1').text.replace("'", "''")
                    cats = list()
                    log.debug('get cats')
                    for cat in recipe_soup.find_all('meta', attrs={'itemprop': 'recipeCategory'}):
                        cats.append(cat.get('content').strip())
                    if not cats:
                        cats.append('uncategorized')
                    query = ("""INSERT INTO parsed_en (prep_time, cook_time, serving, ingredients, title, img_url, 
                             steps, description, categories) VALUES ({}, {}, {}, array{}, '{}', '{}', array{}, '{}', 
                             array{})""").format(prep_time, cook_time, servings, ingredients, title, img_url, steps,
                                                 desc, cats)
                    log.debug(query)
                    log.info('execute query')
                    cursor.execute(query)
                    conn.commit()
                except Exception as e:
                    log.error("parse recipe error = " + str(e))
                finally:
                    cursor.close()
        except Exception as e:
            log.error("parse hrefs error = " + str(e))


if __name__ == "__main__":
    try:
        init()
        parse_fields('')
    except Exception as e:
        log.error("main error = " + str(e))